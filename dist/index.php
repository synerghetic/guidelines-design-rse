<?php
$uri = $_SERVER['REQUEST_URI'];
$path = './prod/guideline/1-impact-utilisateur.html';
if (file_exists('./prod/guideline'.$uri.'.html')) {
    $path = './prod/guideline'.$uri.'.html';
}
if ($uri == '/synerghetic') {
    $path = './prod/synerghetic.html';
}
function callback($buffer) {
    global $path;
    $source = '<main id="js-contentContainer">';
    $content = file_get_contents($path);
    return str_replace($source, $source.$content, $buffer);
}
ob_start("callback");
include('./prod/index.html');
// $cacheFolder = $_SERVER['DOCUMENT_ROOT'].'/cache';
// if (!file_exists($cacheFolder)) {
//     mkdir($cacheFolder);
// }
// $cached = fopen($cacheFolder.(strlen($uri) == 1 ? "/home" : $uri).".html", 'w');
// fwrite($cached, ob_get_contents());
// fclose($cached);
ob_end_flush();
?>