# Guidelines de design RSE - Synerg'hetic

## Configure the project
This project uses parcel. You have to install it globally in order to run the code.
```
npm install -g parcel
```

## Develop
If Parcel is installed globally, you just have to run the following command to launch the project.
```
npm run dev
```

## Build the project
If Parcel is installed globally, run the following command to build the project
```
npm run build
```