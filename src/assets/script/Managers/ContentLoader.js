import Page from "../Pages/Page"

const PAGES = [
    {
        id: 0,
        file: "guideline/1-impact-utilisateur",
        initializer: () => ( new Page() )
    },
    {
        id: 1,
        file: "guideline/2-compression-assets",
        initializer: () => ( new Page() )
    },
    {
        id: 2,
        file: "guideline/3-retrocompatibilite-technologique",
        initializer: () => ( new Page() )
    },
    {
        id: 3,
        file: "guideline/4-accessibilite-des-interfaces",
        initializer: () => ( new Page() )
    },
    {
        id: 4,
        file: "guideline/5-diversite-du-design",
        initializer: () => ( new Page() )
    },
    {
        id: 5,
        file: "synerghetic",
        initializer: () => ( new Page() )
    }
]

export default class ContentLoader {
    
    constructor(_app) {
        this.app = _app
        this.container = document.querySelector('#js-contentContainer')
        this.pageObject = new Page()
        window.addEventListener('popstate', _ => document.location.reload())
    }

    loadViaPathName(_pathName, _data, _autoinit = true) {
        const destination = PAGES.filter(_page => ("/" + _page.file == _pathName))
        if (destination.length > 0) {
            return this.load(destination[0], _data, _autoinit)
        } else {
            return this.loadViaIndex(0, _data, _autoinit)
        }
    }

    loadViaIndex(_index, _data, _autoinit = true) {
        if (_index >= PAGES.length) return
        if (_index < 0) return
        return this.load(PAGES[_index], _data, _autoinit)
    }

    load(_page, _data, _autoinit) {
        this.app.currentStep = _page.id
        this.app.loading = true
        return fetch(`/prod/${_page.file}.html`)
                .then(resp => resp.text())
                .then(html => {
                    if (_autoinit) this.intializePage(html, _page, _data)
                    this.app.loading = false
                    return {
                        html: html,
                        page: _page
                    }
                })
                .catch(error => {
                    console.log(`Error loading page (${_page.file}) : ${error}`)
                    this.app.loading = false
                })
    }

    intializePage(_html, _page, _data) {
        this.container.innerHTML = _html
        this.pageObject.deinit()
        this.pageObject = _page.initializer()
        this.pageObject.init(_data.scrollManager)
        // Update page url
        window.history.pushState(null, null, `/${_page.file}`)
        // document.location.href = `/${_page.file}`
        // console.log(this.pageObject)
    }

}