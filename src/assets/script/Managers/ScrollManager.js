const SCROLL_RATIO = 2

export default class ScrollManager {

    constructor(_container, _grid, _contentLoader, _app) {
        this.container = _container
        this.grid = _grid
        this.contentLoader = _contentLoader
        this.app = _app
        this.waitingData = null
        this.loadingDirection = null
        this.transitionProgression = 0
        this.virtual = {
            scrollValue: _ => (this.scrollValue * SCROLL_RATIO)
        }
        this.onResize()
        if (this.app.currentStep == 0) {
            this.realScrollValue = 0
            this.scrollValue = 0
        } else {
            this.scrollTo(this.screenSize / SCROLL_RATIO, 40)
        }
        window.addEventListener('resize', _ => this.onResize())
        this.loop()
    }

    scrollTo(_dest, _offset = 0) {
        this.realScrollValue = _dest
        this.scrollValue = this.realScrollValue + _offset
        window.scrollTo(this.realScrollValue, this.realScrollValue)
    }

    onResize() {
        this.screenWidth = window.innerWidth
        this.screenHeight = window.innerHeight
        this.screenSize = this.screenWidth
        this.totalScrollLength = this.container.offsetWidth
        this.realScrollLength = this.container.offsetWidth - this.screenSize
        document.body.style.height = `${window.innerHeight + this.totalScrollLength}px`
        document.body.style.width = `${window.innerWidth + this.totalScrollLength}px`
    }

    newPageDidLoad(_e) {
        this.waitingData = _e
    }

    loop() {
        requestAnimationFrame(_ => this.loop())
        if (this.loadingDirection != null) {
            this.loadingTransition()
            return
        }
        this.realScrollValue += (window.scrollY + window.scrollX) / 2 - this.realScrollValue
        window.scrollTo(this.realScrollValue, this.realScrollValue)
        if (this.scrollValue == this.realScrollValue) return
        // Computing
        this.scrollValue = this.scrollValue + (this.realScrollValue - this.scrollValue) * 0.03
        this.scrollValue = Math.abs(this.scrollValue - this.realScrollValue) < 0.5 ? this.realScrollValue : this.scrollValue
        if (this.app.currentStep == 5) {
            this.scrollValue = Math.min(this.scrollValue, this.screenSize / SCROLL_RATIO)
        }
        // Animation
        const prog = this.virtual.scrollValue()/this.screenSize
        const currentBlock = Math.round(prog-.25)
        this.contentLoader.pageObject.updateBlocks(currentBlock)
        this.grid.noTransition()
        if (this.app.currentStep > 0 && this.virtual.scrollValue() <= this.screenSize) {
            this.container.style.transform = `translateX(-${this.screenSize}px)`
            const progression = 1 - (this.virtual.scrollValue() / this.screenSize)
            this.grid.reverseTransition(progression)
            if (progression >= 0.5) {
                this.updateLoadingDirection('previous', progression)
                this.contentLoader.loadViaIndex(this.app.currentStep - 1, {scrollManager: this}, false)
                    .then(_e => this.newPageDidLoad(_e))
            }
        } else if (this.virtual.scrollValue() > this.realScrollLength) {
            this.container.style.transform = `translateX(-${this.realScrollLength}px)`
            const progression = (this.virtual.scrollValue() - this.realScrollLength) / this.screenSize
            this.grid.transition(progression)
            if (progression >= 0.5) {
                this.updateLoadingDirection('next', progression)
                this.contentLoader.loadViaIndex(this.app.currentStep + 1, {scrollManager: this}, false)
                    .then(_e => this.newPageDidLoad(_e))
            }
        } else {
            this.container.style.transform = `translateX(-${this.virtual.scrollValue()}px)`
            this.grid.intro(this.virtual.scrollValue() / this.screenSize)
        }
    }

    loadingTransition() {
        if (this.loadingDirection == 'next') {
            this.transitionProgression = Math.min(1, this.transitionProgression + 0.015)
            this.grid.transition(this.transitionProgression)
        } else if (this.loadingDirection == 'previous') {
            this.transitionProgression = Math.min(1, this.transitionProgression + 0.015)
            this.grid.reverseTransition(this.transitionProgression)
        } else if (this.loadingDirection == 'previous-loaded' && !this.app.loading) {
            this.transitionProgression = Math.min(1, this.transitionProgression + 0.015)
            this.grid.reverseTransition(1 - this.transitionProgression)
            if (this.transitionProgression == 1) this.updateLoadingDirection(null, 0, true)
        } else if (this.loadingDirection == 'next-loaded' && !this.app.loading) {
            this.transitionProgression = Math.min(1, this.transitionProgression + 0.015)
            this.grid.transition(1 - this.transitionProgression)
            if (this.transitionProgression == 1) this.updateLoadingDirection(null, 0, true)
        }
        if (this.transitionProgression == 1) {
            this.updateLoadingDirection(this.loadingDirection + '-loaded', 0, true)
            this.contentLoader.intializePage(this.waitingData.html, this.waitingData.page, {scrollManager: this})
            this.waitingData = null
        }
    }

    updateLoadingDirection(_direction, _progression, _reinitScroll = false) {
        this.loadingDirection = _direction
        this.transitionProgression = _progression
        if (_reinitScroll) {
            this.scrollTo(this.screenSize / SCROLL_RATIO)
            this.container.style.transform = `translateX(-${this.virtual.scrollValue()}px)`
        }
    }

}