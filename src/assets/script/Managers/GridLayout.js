export default class GridLayout {

    constructor() {
        this.grid = document.querySelector('#js-grid')
        this.lines = this.grid.querySelectorAll('div')
    }

    intro(progression) {
        this.lines[3].style.transform = `translateY(${300 * -progression}%)`
    }

    transition(progression) {
        this.lines[2].style.transform = `translateY(${100 * -progression}%) translateY(${24 * progression}px)`
    }

    reverseTransition(progression) {
        if (progression > 0.002) {
            this.grid.classList.add('goback')
        }
        this.lines[0].style.transform = `translateY(${100 * progression}vh) translateY(${-104 * progression}px)`
    }

    noTransition() {
        this.grid.classList.remove('goback')
    }

}