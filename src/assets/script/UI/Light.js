export default class Light {

    constructor(_elems, _scrollManager) {
        this.elems = _elems
        this.paths = document.querySelectorAll('#js-path path')
        this.paths.forEach(_path => {
            _path.length = _path.getTotalLength()
        })
        this.scrollManager = _scrollManager
        this.onResize()
        this.loop()
    }

    loop() {
        requestAnimationFrame(_ => this.loop())
        if (this.paths.length == 0) {
            console.warn('this.paths is empty')
            return
        }
        const p = this.scrollManager.virtual.scrollValue() / this.scrollManager.screenSize
        const path = this.paths[Math.floor(p)]
        let point
        if (path) {
            const length = (p % 1) * path.length
            point = path.getPointAtLength(length)
        } else {
            point = this.paths[4].getPointAtLength(this.paths[4].length)
        }
        const x = point.x % 1440 / 1440 * this.scrollManager.screenWidth
        const y = point.y / 900 * this.scrollManager.screenHeight
        this.elems.forEach(_elem => {
            const offsetX = _elem.isInBlock ? this.scrollManager.virtual.scrollValue() - _elem.offsetX : 0
            _elem.style.transform = `translate(${x + offsetX}px, ${y}px) scale(${0.1 + 1.8 * Math.abs(p % 1 - 0.5)})`
        })
    }

    onResize() {
        this.elems.forEach(_elem => {
            _elem.isInBlock = _elem.parentElement.classList.contains('block')
            if (_elem.isInBlock) {
                _elem.offsetX = _elem.parentElement.offsetLeft
            }
        })
    }

}