import Light from "../UI/Light"

export default class Page {

    constructor() {
        console.log("Construct")
        this.onResizeHandler = _ => this.onResize()
        window.addEventListener('resize', this.onResizeHandler)
    }

    init(_scrollManager) {
        console.log("Init")
        this.light = new Light(document.querySelectorAll('.js-light'), _scrollManager)
        this.blocks = document.querySelectorAll('.block') ?? []
    }

    deinit() {
        console.log("Deinit")
        window.removeEventListener('resize', this.onResizeHandler)
    }

    onResize() {
        this.light.onResize()
    }

    updateBlocks(_index) {
        if (this.blocks == undefined) { return }
        this.blocks.forEach( (block, i) => {
            if (i < _index) {
                block.classList.add('active')
            } else {
                block.classList.remove('active')
            }
        })
    }

}