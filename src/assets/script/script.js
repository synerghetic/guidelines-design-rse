import ContentLoader from "./Managers/ContentLoader"
import GridLayout from "./Managers/GridLayout"
import ScrollManager from "./Managers/ScrollManager"

const container = document.querySelector('#js-appContainer')

const i = document.location.pathname[11]
const app = {
    currentStep: i == "c" ? 5 : parseInt(i) - 1,
    loading: false
}
const contentLoader = new ContentLoader(app)

const grid = new GridLayout()
const scrollManager = new ScrollManager(container, grid, contentLoader, app)

console.warn('PROD : Remove js page load on start')
contentLoader.loadViaPathName(window.location.pathname, { scrollManager: scrollManager })